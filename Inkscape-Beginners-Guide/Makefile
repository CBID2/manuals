# Make text editors behave similarly
# -*- mode: gnumakefile; tab-width: 4; indent-tabs-mode: t; -*-
# vim: tabstop=4
#
# Makefile for Inkscape Beginner's Guide

# help text
define HELP_TEXT
Custom Targets
==============
  update-translation
              to update or create translation po files for a specific language
              Run as SPHINXLANG=<LANGUAGE_CODE> make update-translation
endef

# You can set these variables from the command line.
SPHINXOPTS    =
SPHINXBUILD   = sphinx-build
SPHINXINTL    = sphinx-intl
SPHINXPROJ    = InkscapeBeginnersGuide
SOURCEDIR     = source
BUILDDIR      = build

# check if $SPHINXBUILD exists
.SPHINXBUILD_EXISTS:
	@if ! which $(SPHINXBUILD) > /dev/null 2>&1; then \
		echo -e >&2 \
			"The '$(SPHINXBUILD)' command was not found.\n"\
			"Make sure you have Sphinx installed, then set the SPHINXBUILD environment variable to\n"\
			"point to the full path of the '$(SPHINXBUILD)' executable.\n"\
			"Alternatively you can add the directory with the executable to your PATH.\n"\
			"If you don't have Sphinx installed, grab it from http://sphinx-doc.org)\n"; \
		false; \
	fi

export HELP_TEXT
# Put it first so that "make" without argument is like "make help".
help:
	@echo "Sphinx"
	@echo "======"
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
	@echo "$$HELP_TEXT"

clean:
	rm -rf build/

update-translation:
	@$(SPHINXINTL) update -p $(BUILDDIR)/gettext -l $(SPHINXLANG)

.PHONY: help clean update-translation Makefile

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile .SPHINXBUILD_EXISTS
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
