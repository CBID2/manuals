********
Overview
********

Now you have familiarized yourself with the basics of the Inkscape program. While Inkscape may appear complex when you first use it, with some practice, you will soon feel more confident.

In this chapter, you will discover functionality that can be used to automate or simplify many steps. However, don't forget that originality comes from your imagination, and not from a software that autogenerates the results for you.

First, we will learn about some useful techniques, then we will get to see some interesting filters, extensions and path effects. Lastly, you will learn a little about customizing Inkscape.
